# PNG figure generation from SVG inputs
# =====================================
#
# Requires:
# 	Inkscape for svg to png generation
#
# Use:
# 	ln -s "path to original" makefile
#
# Run:
# 	make
# 	make all
# 	make part
# 	make clean
# 	make clean-part
# 	make clean-whole

IMG_PATH = images
SVG := $(wildcard $(IMG_PATH)/*.svg)
PNG_IMAGE := $(SVG:%.svg=%.png)
DOC := $(wildcard *-micro.md)
PDF := $(DOC:%.md=%.pdf)

.PHONY: all doc png images clean-doc clean-images clean

all: images doc

doc: $(PDF) images

$(PDF): $(DOC)
	pandoc -o $(PDF) --from=markdown+link_attributes --latex-engine=xelatex -V mainfont='Linux Libertine O' $(DOC)

images: $(PNG_IMAGE) $(SVG)

%.png: %.svg
	inkscape --export-png=$*.png --export-area-drawing $*.svg

clean: clean-doc clean-images

clean-doc:
	rm -f $(PNG)

clean-images:
	rm -f $(PNG_IMAGE)
