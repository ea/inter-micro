/* set_the_time
 *
 * Helper sketch to set a custom time on a DS1307
 * (such as the one on the Red-Peg board).
 * Type the date and time into the serial monitor, in the
 * format: T YYYY MM DD hh mm ss
 * Will then report the current time every 10 seconds
 *
 * Requires the Adafruit RTClib library
 *
 */

#include <Wire.h>
#include <RTClib.h>

RTC_DS1307 rtc;

#define SENSOR_ACTIVE_PIN 6
#define READING_DELAY 10000
#define BAUD 115200

uint32_t last_reading = READING_DELAY;

void setup()
{
  Serial.begin(BAUD);
  Serial.println(F("starting set_the_time"));

  // check we have an RTC attached
  if (!rtc.begin()) {
    Serial.println(F("no RTC"));
    die();
  }
  reportTimeNow();

  Serial.println(F("To set the time send \"T YYYY MM DD hh mm ss\""));
}

void loop()
{
  if (Serial.available()) {
    // if we have serial input, check input for setting the time
    setTime();
  } else if (millis() - last_reading >= READING_DELAY) {
    // otherwise read out the time at regular intervals
    reportTimeNow();
    last_reading = millis();
  }
}

void setTime() {
  char c = Serial.read();
  if (c == 't' || c == 'T') {
    uint16_t year;
    uint8_t month, day, hour, min, sec;
    Serial.print(F("Year: "));
    year = Serial.parseInt();
    Serial.println(year);
    Serial.print(F("Month: "));
    month = Serial.parseInt();
    Serial.println(month);
    Serial.print(F("Day: "));
    day = Serial.parseInt();
    Serial.println(day);
    Serial.print(F("Hour: "));
    hour = Serial.parseInt();
    Serial.println(hour);
    Serial.print(F("Minute: "));
    min = Serial.parseInt();
    Serial.println(min);
    Serial.print(F("Second: "));
    sec = Serial.parseInt();
    Serial.println(sec);
    while (Serial.available()) {
      // eat any remaining characters
      Serial.read();
    }
    rtc.adjust(DateTime(year, month, day, hour, min, sec));
    Serial.print(F("RTC set to: "));
    reportTimeNow();
    last_reading = millis();
  }
}

void die() {
  pinMode(LED_BUILTIN, OUTPUT);
  while(1) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  }
}

void reportTimeNow() {
  DateTime now = rtc.now();
  char buf[5];
  sprintf(buf, "%04d", now.year());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.month());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.day());
  Serial.print(buf);
  Serial.write('T');
  sprintf(buf, "%02d", now.hour());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.minute());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.second());
  Serial.print(buf);
  Serial.write('Z');
  Serial.println();
}
