/* sensor_blink
 *
 * toggle the sensor active pin on Red-Peg board,
 * without using the red_peg library
 *
 */

#define SENSOR_ACTIVE_PIN 6
#define BAUD 115200

void setup() {
  Serial.begin(BAUD);
  Serial.println(F("start sensor_blink"));
  delay(100);
  pinMode(SENSOR_ACTIVE_PIN, OUTPUT);
}

void loop() {
  digitalWrite(SENSOR_ACTIVE_PIN, HIGH);
  delay(1000);
  digitalWrite(SENSOR_ACTIVE_PIN, LOW);
  delay(1000);
}
