#include <Wire.h>
#include <MCP342x.h>

/* 
 Read a TMP36 sensor from MCP3428, channel 3
 */

#define BAUD 115200

// 0x68 is the default address for all MCP342x devices
// on Red-Peg, the MCP3428 address is set to 0x6F
uint8_t address = 0x6F;
MCP342x adc = MCP342x(address);

void setup(void)
{
  Serial.begin(BAUD);
  Serial.println("starting temp_mon");
  Wire.begin();

  // Reset devices
  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms
  
  // Check device present
  Wire.requestFrom(address, (uint8_t)1);
  if (!Wire.available()) {
    Serial.print("No device found at address ");
    Serial.println(address, HEX);
    while (1)
      ;
  } else {
    Serial.println("MCP3248 found");
  }

}

void loop(void)
{
  long value = 0;
  MCP342x::Config status;
  // Initiate a conversion; convertAndRead() will wait until it can be read
  uint8_t err = adc.convertAndRead(MCP342x::channel3, MCP342x::oneShot,
           MCP342x::resolution16, MCP342x::gain1,
           1000000, value, status);
  if (err) {
    Serial.print("Convert error: ");
    Serial.println(err);
  }
  else {
    float voltage = value * (2.048 / 32767.0);
    float temp = (voltage-0.5) * 100;

    Serial.print("Value: ");
    Serial.print(value);
    Serial.print(", ");
    Serial.print(voltage);
    Serial.print("V, ");
    Serial.print(temp);
    Serial.print(" degC");
    Serial.println();
  }
  
  delay(1000);
}

