/* rp_sensor_active
 *
 * toggles the sensor power and slave microcontroller
 * on and off every second
 * if you attach an LED + resistor across the
 * 0–5V port, it will light when the port powers on
 */

#include <red_peg.h>
red_peg RP;

void setup() {
  RP.begin();
  delay(100);
  Serial.begin(BAUD);
  Serial.println(F("start rp_sensor_active"));
}

void loop() {
  RP.sensorsOn();
  Serial.print("ON...");
  delay(1000);
  RP.sensorsOff();
  Serial.println("OFF");
  delay(1000);
}
