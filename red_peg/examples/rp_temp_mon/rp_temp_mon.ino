/* rp_temp_mon
 *
 * Take a reading from a TMP36 sensor in the 0–2V port
 * on the red peg board and report it to the Serial monitor
 *
 */

#include <red_peg.h>
red_peg RP;


void setup() {
  RP.begin();
  RP.sensorsOn();
  delay(100);
  Serial.begin(BAUD);
  Serial.println(F("start rp_temp_mon"));
}

void loop() {
  t_SensorData temp = RP.get(TMP);
  if (temp.sensor == TMP) {
    Serial.print("Value: ");
    Serial.print(temp.reading);
    Serial.print(", ");
    Serial.print(RP.volts(temp));
    Serial.print("V, ");
    Serial.print(RP.degC(temp));
    Serial.print(" degC");
    Serial.println();
  } else {
    Serial.println("no return from TMP");
  }
  delay(5000);
}
