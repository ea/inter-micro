#include <Lewis.h>

Lewis Morse;

void setup() {
  // receive on pin 2, transmit on pin 13, 10 WPM
  Morse.begin(2, 13, 10);
}

void loop() {
  Morse.write('k');
  delay(2000);
}
