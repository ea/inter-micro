/* rp_maxbotix
 *
 * Reading the MB 7060 sensor to give distance readings
 *
 */

#include <red_peg.h>
red_peg RP;


void setup() {
  delay(1000);
  RP.begin();
  delay(100);

  Serial.begin(BAUD);
  Serial.println("starting rp_maxbotix");

}

void loop() {
  RP.sensorsOn();
  // wait long enough for the ultrasound to start up
  delay(500);
  t_SensorData depth = RP.get(ANA);
  RP.sensorsOff();

  if (depth.sensor == ANA) {
    RP.print_data(depth);
    Serial.print("Value: ");
    Serial.print(depth.reading);
    Serial.print(", ");
    Serial.print(RP.volts(depth));
    Serial.print(" V, ");
    Serial.print(RP.distance(depth));
    Serial.print(" cm");
    Serial.println();
    delay(5000);
  }
}
