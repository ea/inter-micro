/* rp_ma4_20
 *
 * Take readings from the 4–20mA sensor port and show the
 * ADC reading, mA value and equivalent 0–7000mm level reading
 *
 */

#include <red_peg.h>
red_peg RP;


void setup() {
  RP.begin();
  delay(100);
  Serial.begin(BAUD);
  Serial.println("starting rp_ma4_20");
}

void loop() {
  RP.sensorsOn();
  delay(100);
  t_SensorData depth = RP.get(MA4_20);
  RP.sensorsOff();

  if (depth.sensor == MA4_20) {
    Serial.print("Value: ");
    Serial.print(depth.reading);
    Serial.print(", ");
    Serial.print(RP.mA(depth));
    Serial.print(" mA, ");
    Serial.print(RP.level(depth, 7000L));
    Serial.print(" mm");
    Serial.println();
    delay(1000);
  }
}
