/* rp_rtc_ds1307
 *
 * Read the date and time from the Red-Peg onboard RTC
 * report over serial.
 *
 */
#include <red_peg.h>
red_peg RP;

void setup () {
  RP.begin();
  RP.sensorsOn();
  delay(100);
  Serial.begin(BAUD);
  Serial.println(F("start rp_rtc_ds1307"));
}

void loop () {
  t_SensorData the_time = RP.get(RTC);
  if (the_time.sensor == RTC) {
    // print the current time in ISO 8601 format
    Serial.print(the_time.y);
    Serial.print("-");
    Serial.print(the_time.m);
    Serial.print("-");
    Serial.print(the_time.d);
    Serial.print("T");
    Serial.print(the_time.hh);
    Serial.print(":");
    Serial.print(the_time.mm);
    Serial.print(":");
    Serial.print(the_time.ss); // DS1307 has whole secconds only
    // assume UTC timestamp
    Serial.print("Z");
    Serial.println();
  } else {
    Serial.println(F("no RTC message received. \r\nLast message is:"));
    RP.print_data(the_time);
  }
  delay(10000);
}
