#include <inttypes.h>
#include "Arduino.h"

#include "red_peg.h"

// master only functions

void red_peg::begin(bool sd_write, uint8_t ss_pin, bool report_serial)
{
  // set the slave select pin
  _ss_pin = ss_pin;
  _sd_write = sd_write;
  pinMode(_ss_pin, OUTPUT);
  digitalWrite(_ss_pin, HIGH);
  pinMode(RP_SDI_PIN, INPUT);
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV8);
  if (report_serial == true) {
    Serial.begin(BAUD);
  }
}

t_SensorData red_peg::ask(sensor_e request, uint8_t _y, uint8_t _m, uint8_t _d, uint8_t _hh, uint8_t _mm, uint8_t _ss, uint16_t _the_reading) //, float _the_data)
{
  // create the temporary buffers for send/recieve
  t_SensorData buffer;
  uint8_t* outgoing = (uint8_t*)&buffer;
  t_SensorData response;
  uint8_t* incoming = (uint8_t*)&response;

  // request a specific sensor's data
  buffer.the_command = request;
  // load the rest of the buffer with nothings
  buffer.y= _y;
  buffer.m = _m;
  buffer.d = _d;
  buffer.hh = _hh;
  buffer.mm = _mm;
  buffer.ss = _ss;
  buffer.the_reading = _the_reading;
  //buffer.the_data = _the_data;

  // send the buffer and receive the incoming
  digitalWrite(_ss_pin, LOW);
  delay(TRANSFER_DELAY);
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  for (uint8_t i=0; i<size_t_SensorData; i++) {
    incoming[i] = SPI.transfer(outgoing[i]);
    delayMicroseconds(TRANSFER_DELAY);
  }
  digitalWrite(_ss_pin, HIGH);
  SPI.endTransaction();

  // and reply with the return
  return response;
}

t_SensorData red_peg::get(sensor_e request)
{
  // wrapper to ask and check for a valid reply
  // first, send a request but assume no valid reply
  t_SensorData incoming = ask(request);
  // then send some empty asks until we get a better return
  // or we exceed the expected buffer length
  delay(PROCESSING_DELAY*2);
  //delay(100);
  for (uint8_t i=0; i<BUFFER_LEN+1; i++) {
    if (incoming.the_sensor == request) {
      return incoming;
    }
    // send an empty get
    incoming = ask();
    delay(PROCESSING_DELAY);
  }
  // otherwise indicate an empty response
  //Serial.print(F("no valid return"));
  incoming.the_sensor = EMPTY;
  return incoming;
}

void red_peg::print_data(t_SensorData data_record)
{
  // helper function to write out the data to serial
  Serial.print("0x");
  Serial.print(data_record.start_byte, HEX);
  Serial.print(F(","));
  Serial.print(sensor_message(data_record.the_sensor));
  Serial.write(',');
  Serial.print(data_record.y);
  Serial.write('-');
  if (data_record.m < 10) { Serial.write('0'); }
  Serial.print(data_record.m);
  Serial.write('-');
  if (data_record.d < 10) { Serial.write('0'); }
  Serial.print(data_record.d);
  Serial.write('T');
  if (data_record.hh < 10) { Serial.write('0'); }
  Serial.print(data_record.hh);
  Serial.write(':');
  if (data_record.mm < 10) { Serial.write('0'); }
  Serial.print(data_record.mm);
  Serial.write(':');
  if (data_record.ss < 10) { Serial.write('0'); }
  Serial.print(data_record.ss);
  Serial.write('Z');
  Serial.write(',');
  Serial.print(data_record.the_reading);
  //Serial.write(',');
  //Serial.print(data_record.the_data);
  Serial.print(",0x");
  Serial.print(data_record.end_byte, HEX);
  Serial.println();
}

char* red_peg::sensor_message(sensor_e request)
{
  if (request == OK) {
    return "OK";
  } else if (request == AVAILABLE) {
    return "AVAILABLE";
  } else if (request == EMPTY) {
    return "EMPTY";
  } else if (request == RTC) {
    return "RTC";
  } else if (request == ADC1) {
    return "ADC1";
  } else if (request == ADC2) {
    return "ADC2";
  } else if (request == ADC3) {
    return "ADC3";
  } else if (request == SDI_12) {
    return "SDI_12";
  } else if (request == SET_RTC) {
    return "SET_RTC";
  } else if (request == SDI_12_FLOAT) {
    return "SDI_12_FLOAT";
  } else if (request == SDI_12_SCAN) {
    return "SDI_12_SCAN";
  } else if (request == VERSION) {
    return "VERSION";
  } else {
    return "UNKNOWN";
  }
}

void red_peg::sensorsOn()
{
  // setup the power control pin
  pinMode(SENSOR_ACTIVE_PIN, OUTPUT);
  // turn it on
  digitalWrite(SENSOR_ACTIVE_PIN, HIGH);
}

void red_peg::sensorsOff()
{
  // turn it off
  digitalWrite(SENSOR_ACTIVE_PIN, LOW);
  // switch to low impedance
  //pinMode(SENSOR_ACTIVE_PIN, INPUT);
}

float red_peg::degC(t_SensorData data_record)
{
  if (data_record.the_sensor == TMP) {
    // convert 0-32767 reading to 10mV/degC conversion w/ 0.5V offset from zero
    return float( ((data_record.the_reading * (2.048/32767.0)) - 0.5) * 100.0 );
  } else {
    return -1.0;
  }
}

float red_peg::volts(t_SensorData data_record)
{
  if (data_record.the_sensor == ADC1 || data_record.the_sensor == ADC3) {
    return float( data_record.the_reading * (2.048/32767.0) );
  } else if (data_record.the_sensor == ADC2) {
    return float( data_record.the_reading * (5.0/32767.0) );
  } else {
    return -1.0;
  }
}

float red_peg::mA(t_SensorData data_record)
{
  if (data_record.the_sensor == MA4_20) {
    return float( data_record.the_reading * 20.0 / float(MAX_20MA) );
  } else {
    return -1.0;
  }
}

float red_peg::level(t_SensorData data_record, double max_level)
{
  float _level = float(max_level);
  return level(data_record, _level);
}

float red_peg::level(t_SensorData data_record, float max_level)
{
  if (data_record.the_sensor == MA4_20) {
    // subtract the minimum level value
    long reading = long(data_record.the_reading) - MIN_4MA;
    float level = float(reading) * ( max_level / float(MAX_20MA - MIN_4MA) );
    return level;
  } else {
    return -max_level;
  }
}

long red_peg::level(t_SensorData data_record, int max_level)
{
  long _level = long(max_level);
  return level(data_record, _level);
}

long red_peg::level(t_SensorData data_record, long max_level)
{
  if (data_record.the_sensor == MA4_20) {
    long level = map(data_record.the_reading, MIN_4MA, MAX_20MA, 0, max_level);
    return level;
  } else {
    return -max_level;
  }
}

long red_peg::distance(t_SensorData data_record, usound_e sensor_type)
{
  long max_distance = 765L; // default MX7060 is 0-765cm
  if (sensor_type == MB7366) { max_distance = 10240L; } // MB7366 is 0–10,240mm
  if (data_record.the_command == ANA) {
    long distance = map(data_record.the_reading, 0, MAX_20MA, 0, max_distance);
    return distance; // in mm or cm
  } else {
    return -1L;
  }
}
