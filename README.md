# Indermediate Microcontrollers

Course materials for the Intermediate Microcontrollers course.

 * [Course text](inter-micro.md)
 * [Example sketches](red_peg/examples/)

Select the `Download` icon and `Download Zip` to get a local copy of these materials.

To install the examples and the red_peg library, copy the `red_peg`
folder into your Arduino Sketchbook folder (eg. `My Documents/Arduino/libraries`).
Then close all open Arduino IDE windows and reopen. The examples will be listed
under `File → Examples → Red-Peg`.

## Required libraries

To run all the examples, you may need to install other dependent libraries.

Some are available in the Arduino Libraries manager:

 * [Lewis](github.com/DefProc/Lewis) to run `a_morse`,
 * [MCP342x](github.com/stevemarple/MCP342x) to run `b_temp_mon`,
 * [RTClib](github.com/adafruit/RTClib) to run `set_the_time`,
 * [SdFat](github.com/greiman/SdFat) to run `sd_read_write` and `rp_sd_logger`.
